## Qualtrics and REDCap

### Aidan Wilson
Research Consultant, eResearch Analyst
aidan.wilson@acu.edu.au

---

## Outline

- Electronic data capture |
- Qualtrics |
- REDCap |
- Live demo |

Note:
This presentation will discuss the two most popular tools we have at ACU for collecting electronic data. Qualtrics and REDCap.

I'll begin by giving a bit of an overview of electronic data capture as a concept, and then I'll introduce each tool, describing its background and main features, its strengths and weaknesses and some typical use cases .

Time permitting, I'll switch over to both REDCap and Qualtrics and give you a bit of a sense of what the interface looks like, and what it's like to use either tool.

This presentation won't go into detail about how to use either of these tools; that's a matter for training, and I will mention three training courses coming up around these tools at the end. The idea of this presentation is for researchers, or research administrators and supporters, to be aware of the two tools and how they differ and so which may be more appropriate in what context.

---

### Electronic data capture

Using forms or surveys to collect information from study participants.

- Simple surveys |
- Longitudinal studies |
- Controlled trials |

Note:
For this presentation, I am referring to electronic data capture as a method to collecting data from study participants. Examples might include, but won't be limited to, a simple survey, a longitudinal study following participants over time, or a clinical trial in which at least one group undergoes some intervention, another group does not, and the two are compared. 

+++

### Forms vs Surveys

Forms:

Data is entered into the collection tool by the research personnel based on information provided by the participant, or on behalf of the participant.

Note:
An important distinction is between forms, and surveys. Forms consist of a researcher, or assistant, filling in information provided by the participant, or on behalf of the participant. If someone stops you in the street for a brief questionnaire where they ask you questions and you respond verbally, that data is being recorded in a form. Another example might be an epidemiological study where a hospital makes available deidentified records which include demographic details and medical observations while they were admitted, which the researcher enters into a database before analysing it for any trends.

+++

### Forms vs Surveys

Surveys:

Data is entered by the participant themselves into the system, often anonymously after following a link published on a website or social media.

Note:
A survey is basically where the study participant enters the data themselves, such as an anonymous online questionnaire, or a questionnaire as part of a broader study, the results from which are aligned with other data collected via forms.

+++

### Anonymous vs Confidential

Anonymous:

No one, not even the investigator, is able to identify a participant from the data. Usually made possible through the means with which data is collected, and no personal information is collected.

Note:
Another distinction is Anonymous versus Confidential.

Anonymous means no one knows who has participated in the study. For a study to be anonymous, the data collection must take place in a particular manner, and appropriate tools be employed to ensure that no identifiable information, such as the participant's IP address, email, name, etc., is collected. Anonymous data collection usually takes the form of online surveys, but they still have to be set up properly to be fully anonymous.

+++

### Anonymous vs Confidential

Confidential:

Personal information is collected and used to facilitate the study, but is managed in such a way that participants' identities are not revealed to others. This is usually achieved through protecting the data, deidentifying data, aggregating, and so on.

Useful resource on [Anonymity versus Confidentiality](https://www.statisticssolutions.com/what-is-the-difference-between-anonymity-and-confidentiality/).

Note:
Confidential means that personally identifiable information is collected, because it needs to be. For example, in a longitudinal study, the participants need to be able to be contacted by the study team for later data collection, and the data they contribute to the project needs to be aligned to their earlier responses so that proper analysis can take place. The challenge with confidential studies is to manage the data is such a way that you can make assurances to your participants that their information will remain safe.

This means that if you collect data via a telephone or face-to-face interview, you cannot call it anonymous.

It might be a good time to mention that your Ethics approval will require you to tell your participants how data is collected, stored and managed, for any kind of study, and you must inform them of whether it is anonymous or confidential.

With those definitions out of the way I will turn my attention to the two tools that ACU has in place for facilitating electronic data capture.

---

## Qualtrics

+++

### Qualtrics

- Site-license; Free to use for ACU staff and students |
- Developed for market research|
- Lots of effort put into the interface|
- Log in with your university credentials |
- Straightforward interface for designing and distributing surveys |
- SaaS - Data stored in the US |
- Limited options to anonymise data |

Note:
Qualtrics is an online survey design and distribution system which is available to all ACU staff and students for free. 

It was originally developed for market research and business intelligence data collection, but is also used a lot by researchers, most notably psychology, but really any field.

It features an easy-to-use interface to designing a survey and distributing it to participants.

However, Qualtrics does not have the same level of controls over sensitive data as REDCap, and may not be the best choice if you are collecting personal information.

Firstly, Qualtrics is a software-as-a-service offering, meaning you access it using a web-browser and the application and all data is held on Qualtrics servers, which are currently located in US data centres, which is problematic for ARC funded projects, although it is something ACU are looking to fix.

Secondly, Qualtrics provides only limited options to anonymise data. Also as we will see it is not able to handle longitudinal data collection, and is only really suited to simple surveys.

+++

### Qualtrics

Excels in allowing users to quickly build surveys with an emphasis on user experience

- Point, click, drag interface with intuitive controls|
- Display and skip logic|
- Survey flow|
- Value piping|
- Extensive array of question types|
- Contact list management and automation|

Note:
Qualtrics is a very simple program to start using, and has an intuitive, point and click interface with controls that appear as you need them, and more advanced controls available if you start looking. They have clearly spent a lot of effort designing a user interface that works well,

It gives users control over display and skip logic to control whether questions show up or not, and with what's called survey flow, to control how a participant proceeds through whole parts of a survey.

A user can pipe values from the dataset into a question, email or other text area, for better personalisation of the survey experience.

There is an impressive array of question types with useful defaults for lots of different use cases.

Users can also manage contact lists, and even automate their generation from responses to surveys.

+++
![qualtrics_q1](assets/qualtrics_q1.png)

+++
![qualtrics_q2](assets/qualtrics_q2.png)

+++
![qualtrics_q3](assets/qualtrics_q3.png)

+++

### Qualtrics

- Collaborative features are limited|
- No concept of events, or the repetition of a survey|
- No way to link responses from a single participants across time-points|

Note:
Qualtrics falls short on some important features though. For one thing, the features for collaborating are limited. You can share a project with anyone inside or outside ACU, but you can only choose to allow or disallow Edit, View Reports, Activate/Deactivate, Copy, Distribute. What this means is that if someone is allowed to view any data in a qualtrics project, they can view all data in that project.

Qualtrics has a one-to-one relationship between a project and a survey. In other words a survey is a project. This means there is no concept of repeating a survey multiple times over many events, and, somewhat related, no way to link responses from one individual across multiple time points. 

This ultimately makes Qualtrics unsuitable for longitudinal projects.

There are workarounds to this, but they're not great. One workaround is to ask the participant to enter their email address, and the user can:

1. use that email address to invite the participant to subsequent surveys, thus emulating a longitudinal project, and
2. use that email as a primary key to link responses together for data analysis, but this is an extremely unsafe method and requires the project team to inspect the personal information in order to make sense of the data.

Another method to solve this latter problem is to ask respondents to generate their own primary key, such as using the first two letters of their surname, the first letter of their birth month, and the last four digits of their phone number, the idea being that the user will have to fill this in every time the survey is run, or for every survey that is run, and the team can use that code to join responses.

This is better, but it is highly error prone.

+++

### Qualtrics use cases

- Simple surveys|
- Projects without Longitudinal collection|
- Not highly complex user rights requirements|
- Landing pages and 'surveys' that provide information but don't collect data|

Note:
The most typical use case for Qualtrics is a simple survey, but these probably account for the majority of data collection activities. At ACU, Qualtrics is used for a lot of business data collection, such as course evaluation.

Qualtrics should only be used where user rights don't need to be too advanced, most simply if a project only has a single user, like a phd or honours student.

Another use case which is less explored is using Qualtrics as a landing page for providing information that depends on logic, and ironically, where no data collection is necessary. ACU uses Qualtrics in this way for the undergraduate enrolment process. Applicants are sent a link to a survey where they enter some details, like which campus they will be attending, and what sort of social events they are interested in, and are presented with information and hyperlinks to other resources that they can follow up on.

This kind of use case could be used by others too. For example, a decision tree, such as a data management wizard that asks the 'participant' some questions about their data and project, and then it might offer some options based on the information they have entered.

+++
![ask-yp-flow](assets/ask-yp-flow.png)

Note: 
This does not mean you can't build very complex surveys out of multiple Qualtrics projects. Here's a project I helped build a couple of years ago. Using a combination of email list triggers, embedded data fields and query string parameters, this team were able to produce a system of 5 interlinked surveys, each of which is completed by a different person but which make up a single dataset.

Doing this takes a lot of patience, experience, and thorough testing, and honestly if I were to do it all again, I'd probably recommend they use REDCap instead.

---

## REDCap

+++

### REDCap

- Open source application maintained by Vanderbilt University |
- Built expressly for managing clinical trials |
- Very strict terms and conditions for licensees |
- Self-hosted - Data stored on ACU data centres |
- Steep learning curve |
- Difficult interface |

Note:
REDCap is an open source application built and maintained by Vanderbilt University for running clinical trials, but it's also capable of running simple surveys or data collection forms, i.e., where data is entered by the project personnel rather than by the participant themselves. This is pretty standard in clinical trials where a clinician would enter both identifying information for a subject plus relevant medical results into a secure portal.

Vanderbilt keep a very tight control over who can access the software through a license. I won't go into the details, but the purpose is to ensure that REDCap maintains HIPAA compliance. 

REDCap is not SaaS, it is self-hosted, as required by the license. This means the application and all its data resides on premise in ACU's data centres, which is a big positive when it comes to complying with privacy legislation and ARC funding rules.

Unfortunately though, REDCap has a steep learning curve, and its interface is not as intuitive as Qualtrics.

+++

### REDCap

Highly powerful and extensible, with very good control over data and privacy

- Simple interface for building both forms and surveys|
- Offline editing using a spreadsheet editor|
- Branching logic, piping, calculated fields|
- All common field types|
- Excellent control over events, repetition|
- Fine-grained control over user rights and data access|
- Audit trail for any changes made to any data point|

Note:
REDCap is very much a niche product for use in medical and clinical research, and so doesn't concentrate much on looking pretty or having an intuitive interface, instead concentrating on extending the features and maintaining strong control over data and privacy.

It has a simple enough interface for building forms using an online designer, and supports offline editing in Excel or any spreadsheet editor.

Like any EDC application is supports branching logic and piping, but unlike most, it supports calculated fields, allowing the user to configure simple or even very complex calculations within a form or survey. The classic example being to calculate a BMI from height and weight, which would be asked of the participant. The BMI field is then stored and can be used in evaluations, for example, ask a certain question only of people whose BMI is higher than a certain threshold. While this is possible in Qualtrics, it is fairly complicated for a novice user to set up.

REDCap supports all the common field types but does not have the same attention to pre-configured defaults that Qualtrics has.

REDCap fundamentally works very well with events, repetitions, and can manage participants through the events in a Longitudinal study.

Also, REDCap provides users with extremely good control over user rights and access to data, reports and exports, and is quite suited to projects in which a team of people might have different roles on a project, even down to clinical staff in a hospital context that need to enter data for a patient.

+++
![redcap_1](assets/redcap_1.png)

Note:
Here are some examples of REDCap and the features around data security. Here you can see that a user can be given specific access to different functions, and can be allowed to only view certain levels of data. You might also notice that you can limit users to only seeing data which is "de-identified". This raises another great feature of REDCap, which is that when adding fields, you can indicate which fields are identifiers, and marking them as such will mean you can automatically prevent a user or user role from seeing those fields in the data.

+++
![redcap_2](assets/redcap_2.png)

Note:
This screenshot illustrates one key difference between Qualtrics and REDCap, which is that REDCap allows a project to contain many different "instruments", which can be separately made into surveys or left as data entry forms.

+++
![redcap_3](assets/redcap_3.png)

Note:
This means that you can configure events, within which different instruments can be enabled or disabled, allowing the user to build complex longitudinal studies out of a small number of data collection instruments.

+++

### REDCap

- Steep learning curve|
- Interface is not intuitive|
- Some basic features missing|

Note:
As I mentioned several times, REDCap has a steep learning curve, and users typically won't be able to dive in themselves and start using it without first either attending some training, or spending some time reading the built-in help and FAQs.

The interface is not pretty, and while there are some options for users to modify colours and fonts, those options are limited.

Some basic features are missing, like the ability to configure a field that has multiple sub-fields, like an address field with multiple parts, like street number, street name, suburb, etc. While there are usually some external modules available to add these features in, they may be beyond the capabilities of most users.

+++

### REDCap use cases

- Longitudinal projects|
- Projects with complex user access requirements|
- Projects with sensitive data under management|
- Projects where data auditing is necessary|
- Projects with multiple arms|

Note:
It might be pretty clear where the boundary between Qualtrics and REDCap is by now, and that REDCap is suited to the more complex requirements of Longitudinal studies or clinical trials, but it can also do simple surveys, except that it might be like using a canon to kill a fly.

+++
![redcap_uc1](assets/redcap_uc1.png)

Note:
Here's a project that I helped out recently with a fairly complex REDCap project. The project consists of a number of standard measures, each a separate instrument, and a couple of demographic and project related forms.

+++
![redcap_uc2](assets/redcap_uc2.png)

Note:
The lineup of instruments within events may look complicated, but the important thing to note is that the project differentiates important parts of the project into separate events, like enrolment, which includes withdrawal, and the measures themselves which occur at various times, prior to the intervention, immediately after the intervention, and then again at three, six and twelve months. At each of these time points, REDCap will automatically email the participant with an invitation to click a link and commence each event's measures, and, critically for confidentiality, the information about which email address is associated with which individual in the dataset, is entirely under the control of the lead investigator.
---

## Live demo

---

## Recommendations

- Qualtrics is an excellent way to start with EDC|
- Look into a more advanced tool like REDCap only when you need it|
	- Longitudinal|
	- Collecting sensitive data|
	- Requiring much more fine-grained control|

Note:
I would say that researchers should start out using Qualtrics and only start using REDCap when they need to, such as when a project is Longitudinal, when it collects sensitive data, or when fine-grained control over user rights and data access is required.

In general we in eResearch recommend that researchers should be using REDCap for human data collection projects that are deemed high-risk by the HREC. Conversely, if a project is deemed low-risk, then researchers may use Qualtrics. Qualtrics can of course be used for data collection that is not considered research, such as operational or administrative.

---

### Getting help

- [rdcap.acu.edu.au](https://rdcap.acu.edu.au)
- [acu.qualtrics.com](https://acu.qualtrics.com)
- Library, eResearch, Ethics
- Training courses via Intersect
	- [intersect.org.au/training](https://intersect.org.au/training)
	- One introductory Qualtrics course
	- One intro and one advanced REDCap course

Note:
Researchers can get help from the Library, from eResearch, or from Research Ethics. For specific help with either of these tools, I will probably get involved myself as I deeply support both these tools.

As I mentioned at the start, I offer training in both Qualtrics and RE
Qualtrics and REDCap, and I have just organised three courses for early December, which will be run both in person at North Sydney, and online via Zoom. One will be the brand new Qualtrics introductory course, and two will be on REDCap; one introduction for the basics, and one advanced course that goes into detail on building a Longitudinal study.

---

### Thank you

Get these slides at [inter.fyi/qualtrics-redcap/20191113](https://inter.fyi/qualtrics-redcap/20191113)
[Aidan.Wilson@acu.edu.au](mailto:Aidan.Wilson@acu.edu.au)
